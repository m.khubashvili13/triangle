def is_triangle(a, b, c):
    """
    Determine if a valid triangle with nonzero area can be constructed with the given side lengths.
    
    :param a: Length of the first side.
    :param b: Length of the second side.
    :param c: Length of the third side.
    
    :return: True if a triangle can be formed; False otherwise.
    """
    # all three of them should be positive numbers
    if a <= 0 or b <= 0 or c <= 0:
        return False
    
    # The sum of any of the two sides should be greater than the third one
    return a + b > c and a + c > b and b + c > a
